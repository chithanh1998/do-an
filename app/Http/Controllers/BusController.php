<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bus;
use App\Bus_type;
use App\Schedule;

class BusController extends Controller
{
    //
    public function list(){
        $bus = Bus::all();
        $bus_type = Bus_type::all();
        return view('bus.list',['bus'=>$bus, 'bus_type'=>$bus_type]);
    }
    public function type_list(){
        $bus_type = Bus_type::all();
        return view('bus.type',['bus_type'=>$bus_type]);
    }
    public function add_bus(Request $request){
        $bus = Bus::where('license_plate', $request->license)->get()->count();
        if($bus == 1){
            echo "err";
        } else {
        $bus = new Bus;
        $bus->license_plate = $request->license;
        $bus->type = $request->type;
        $bus->import_date = $request->import_date;
        $bus->save();
        echo "ok";
        }
    }
    public function get_edit_bus($id){
        $bus = Bus::find($id);
        echo json_encode($bus);
    }
    public function post_edit_bus(Request $request){
        $bus = Bus::find($request->id);
        $bus->type = $request->type;
        $bus->import_date = $request->import_date;
        $bus->status = $request->status;
        $bus->save();
        echo "ok";
    }
    public function delete_bus($id){
        $bus = Bus::find($id);
        $check = Schedule::where('bus',$id)->count();
        if($check != 0){
            return back()->with('err','Vehicles are in use process');
        } else {
            $bus->delete();
            return back()->with('noti','Delete success');
        }
    }
    public function add_bus_type(Request $request){
        $type = Bus_type::where('type', $request->type)->get()->count();
        if($type == 1){
            echo "err";
        } else {
        $type = new Bus_type;
        $type->type = $request->type;
        $type->save();
        echo "ok";
        }
    }
    public function delete_bus_type($id){
        $type = Bus_type::find($id);
        $check = Bus::where('type',$id)->count();
        if($check != 0){
            return redirect('bus/type')->with('err','Vehicles are in use process');
        } else {
        $type->delete();
        return back()->with('noti','Delete success');
        }
    }
}
