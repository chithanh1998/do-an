<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Bus;
use App\Bus_type;
use App\Drivers;
use App\Schedule;
use App\Users;
use App\Province;

class DashboardController extends Controller
{
    public function month(){
        $data = array();
        $data['total_drivers'] = Drivers::all()->count();
        $data['total_bus'] = Bus::all()->count();
        $data['total_schedule'] = Schedule::all()->count();
        $month = array();
        for ($i=1; $i < 13; $i++) { 
            $mth = Schedule::whereYear('schedule.start_time', '=', Carbon::now()->year)->whereMonth('schedule.start_time', '=', $i)->count();
            array_push($month,$mth);
        }
        $schedule_month = "[{
            y: 'Jan',
            a: ".$month[0].",
            
        }, {
            y: 'Feb',
            a: ".$month[1].",
            
        }, {
            y: 'Mar',
            a: ".$month[2].",
            
        }, {
            y: 'Apr',
            a: ".$month[3].",
            
        }, {
            y: 'May',
            a: ".$month[4].",
            
        }, {
            y: 'Jun',
            a: ".$month[5].",
            
        }, {
            y: 'Jul',
            a: ".$month[6].",
            
        },{
            y: 'Aug',
            a: ".$month[7].",
            
        },{
            y: 'Sep',
            a: ".$month[8].",
            
        },{
            y: 'Oct',
            a: ".$month[9].",
            
        },{
            y: 'Nov',
            a: ".$month[10].",
           
        },{
            y: 'Dec',
            a: ".$month[11].",
            
        }]";
        $data['schedule_month'] = $schedule_month;
        $type = Bus_type::all();
        $rt_type = array();
        $bus_month = DB::table('schedule')->whereMonth('start_time', Carbon::now()->month)->leftjoin('bus','schedule.bus','=','bus.id')->leftjoin('bus_type','bus.type','=','bus_type.id')->select('schedule.*','bus_type.type','bus.type as id_type')->get();
        foreach($type as $tp){
            $count = 0;
            foreach($bus_month as $bm){
                if($bm->id_type == $tp->id){
                    $count++;
                }
            }
            $rt_type[] = ['label' => $tp->type, 'data' => $count];
        }
        $data['ratio_type'] = $rt_type;
        $data['filter'] = 'month';
        $rank_td = DB::table('schedule')->whereMonth('start_time', Carbon::now()->month)->leftjoin('vn_province as from_name','schedule.from','=','from_name.matp')->leftjoin('vn_province as end_name','schedule.to','=','end_name.matp')->groupBy('from_name.name','end_name.name')->select('from_name.name as from','end_name.name as to', DB::raw('count(*) as total'))->orderBy('total', 'desc')->get()->take(3);
        $rank_bus = DB::table('schedule')->whereMonth('start_time', Carbon::now()->month)->leftjoin('bus','schedule.bus','=','bus.id')->groupBy('bus','bus.license_plate')->select('bus.license_plate', DB::raw('count(*) as total'))->orderBy('total', 'desc')->get()->take(3);
        return view('dashboard.index',['data'=>$data,'rank_td'=>$rank_td,'rank_bus'=>$rank_bus]);
    }
    public function year(Request $request){
        $data = array();
        $data['total_drivers'] = Drivers::all()->count();
        $data['total_bus'] = Bus::all()->count();
        $data['total_schedule'] = Schedule::all()->count();
        $schedule_year = array();
        for ($i=$request->start; $i <= $request->end; $i++) { 
            $yr = Schedule::whereYear('schedule.start_time', '=', $i)->count();
            $schedule_year[] = ['y'=>$i, 'a'=>$yr];
        }
        $data['schedule_year'] = $schedule_year;
        $type = Bus_type::all();
        $rt_type = array();
        $bus_month = DB::table('schedule')->whereYear('start_time', '>=', $request->start)->whereYear('start_time', '<=', $request->end)->leftjoin('bus','schedule.bus','=','bus.id')->leftjoin('bus_type','bus.type','=','bus_type.id')->select('schedule.*','bus_type.type','bus.type as id_type')->get();
        foreach($type as $tp){
            $count = 0;
            foreach($bus_month as $bm){
                if($bm->id_type == $tp->id){
                    $count++;
                }
            }
            $rt_type[] = ['label' => $tp->type, 'data' => $count];
        }
        $data['ratio_type'] = $rt_type;
        $data['filter'] = $request->start.' - '.$request->end;
        $rank_td = DB::table('schedule')->whereYear('start_time','>=', $request->start)->whereYear('start_time','<=', $request->end)->leftjoin('vn_province as from_name','schedule.from','=','from_name.matp')->leftjoin('vn_province as end_name','schedule.to','=','end_name.matp')->groupBy('from_name.name','end_name.name')->select('from_name.name as from','end_name.name as to', DB::raw('count(*) as total'))->orderBy('total', 'desc')->get()->take(3);
        $rank_bus = DB::table('schedule')->whereYear('start_time','>=', $request->start)->whereYear('start_time','<=', $request->end)->leftjoin('bus','schedule.bus','=','bus.id')->groupBy('bus','bus.license_plate')->select('bus.license_plate', DB::raw('count(*) as total'))->orderBy('total', 'desc')->get()->take(3);
        return view('dashboard.index',['data'=>$data,'rank_td'=>$rank_td,'rank_bus'=>$rank_bus]);
    }
    public function all(Request $request){
        $data = array();
        $data['total_drivers'] = Drivers::all()->count();
        $data['total_bus'] = Bus::all()->count();
        $data['total_schedule'] = Schedule::all()->count();
        $type = Bus_type::all();
        $rt_type = array();
        $bus_all = DB::table('schedule')->leftjoin('bus','schedule.bus','=','bus.id')->leftjoin('bus_type','bus.type','=','bus_type.id')->select('schedule.*','bus_type.type','bus.type as id_type')->get();
        foreach($type as $tp){
            $count = 0;
            foreach($bus_all as $bm){
                if($bm->id_type == $tp->id){
                    $count++;
                }
            }
            $rt_type[] = ['label' => $tp->type, 'data' => $count];
        }
        $data['ratio_type'] = $rt_type;
        $data['filter'] = 'all';
        $rank_td = DB::table('schedule')->leftjoin('vn_province as from_name','schedule.from','=','from_name.matp')->leftjoin('vn_province as end_name','schedule.to','=','end_name.matp')->groupBy('from_name.name','end_name.name')->select('from_name.name as from','end_name.name as to', DB::raw('count(*) as total'))->orderBy('total', 'desc')->get()->take(3);
        $rank_bus = DB::table('schedule')->leftjoin('bus','schedule.bus','=','bus.id')->groupBy('bus','bus.license_plate')->select('bus.license_plate', DB::raw('count(*) as total'))->orderBy('total', 'desc')->get()->take(3);
        return view('dashboard.index',['data'=>$data,'rank_td'=>$rank_td,'rank_bus'=>$rank_bus]);
    }
}
