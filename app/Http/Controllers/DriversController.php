<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Drivers;

class DriversController extends Controller
{
    //
    public function list(){
        $drivers = Drivers::all();
        return view('drivers.list',['driver'=>$drivers]);
    }
    public function add_driver(Request $request){
        $driver = new Drivers;
        $driver->name = $request->name;
        $driver->date_of_birth = $request->date_of_birth;
        $driver->phone = $request->phone;
        $driver->address = $request->address;
        $driver->save();
        echo "ok";
    }
    public function get_edit_driver($id){
        $drivers = Drivers::find($id);
        echo json_encode($drivers);
    }
    public function post_edit_driver(Request $request){
        $driver = Drivers::find($request->id);
            $driver->name = $request->name;
            $driver->date_of_birth = $request->date_of_birth;
            $driver->phone = $request->phone;
            $driver->address = $request->address;
            $driver->status = $request->status;
            $driver->save();
            echo "ok";
    }
    public function delete_driver($id){
        $driver = Drivers::find($id);
        $driver->delete();
        return back()->with('noti','Delete success');
    }
}
