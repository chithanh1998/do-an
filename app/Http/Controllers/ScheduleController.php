<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Schedule;
use App\Drivers;
use App\Bus;
use Carbon\Carbon;
use App\Province;
use App\Bus_type;

class ScheduleController extends Controller
{
    //
    public function list(){
        $today = Carbon::now()->toDateString();
        if(!empty(session('start_date_filter'))){
            $start = session('start_date_filter');
            $end = session('end_date_filter');
        } else {
            $start = $today;
            $end = $today;
        }
        $bus = Bus::all();
        $driver = Drivers::all();
        $province = Province::all();
        $bus_type = Bus_type::all();
        $data = DB::table('schedule')->whereDate('start_time','>=',$start)->whereDate('start_time','<=',$end)->leftjoin('bus','bus.id','=','schedule.bus')->leftjoin('drivers','drivers.id','=','schedule.driver')->leftjoin('vn_province as from_name','schedule.from','=','from_name.matp')->leftjoin('vn_province as end_name','schedule.to','=','end_name.matp')->select('schedule.*','bus.license_plate as license_plate','drivers.name as driver_name','from_name.name as from_province','end_name.name as end_province')->get();
        return view('schedule.list',['data'=>$data,'bus'=>$bus,'driver'=>$driver,'province'=>$province,'start_date'=>$start,'end_date'=>$end,'bus_type'=>$bus_type]);
    }
    public function list_filter(Request $request){
        session()->put('start_date_filter',$request->start);
        session()->put('end_date_filter',$request->end);
        $bus = Bus::all();
        $driver = Drivers::all();
        $province = Province::all();
        $bus_type = Bus_type::all();
        $data = DB::table('schedule')->whereDate('start_time','>=',$request->start)->whereDate('start_time','<=',$request->end)->leftjoin('bus','bus.id','=','schedule.bus')->leftjoin('drivers','drivers.id','=','schedule.driver')->leftjoin('vn_province as from_name','schedule.from','=','from_name.matp')->leftjoin('vn_province as end_name','schedule.to','=','end_name.matp')->select('schedule.*','bus.license_plate as license_plate','drivers.name as driver_name','from_name.name as from_province','end_name.name as end_province')->get();
        return view('schedule.list',['data'=>$data,'bus'=>$bus,'driver'=>$driver,'province'=>$province,'start_date'=>$request->start,'end_date'=>$request->end,'bus_type'=>$bus_type]);
    }
    public function detail(Request $request){
        $start = Carbon::now()->firstofMonth()->toDateString();
        $today = Carbon::now()->toDateString();
        $bus = Bus::find($request->id);
        $driver = Drivers::all();
        $province = Province::all();
        $bus_type = Bus_type::all();
        $data = DB::table('schedule')->where('bus',$request->id)->whereMonth('start_time','=', Carbon::now()->month)->leftjoin('bus','bus.id','=','schedule.bus')->leftjoin('drivers','drivers.id','=','schedule.driver')->leftjoin('vn_province as from_name','schedule.from','=','from_name.matp')->leftjoin('vn_province as end_name','schedule.to','=','end_name.matp')->select('schedule.*','bus.license_plate as license_plate','drivers.name as driver_name','from_name.name as from_province','end_name.name as end_province')->get();
        return view('schedule.detail',['data'=>$data,'start_date'=>$start,'end_date'=>$today,'license_plate'=>$bus->license_plate,'id'=>$request->id]);
    }
    public function detail_filter(Request $request){
        $bus = Bus::find($request->id);
        $driver = Drivers::all();
        $province = Province::all();
        $bus_type = Bus_type::all();
        $data = DB::table('schedule')->where('bus',$request->id)->whereDate('start_time','>=',$request->start)->whereDate('start_time','<=',$request->end)->leftjoin('bus','bus.id','=','schedule.bus')->leftjoin('drivers','drivers.id','=','schedule.driver')->leftjoin('vn_province as from_name','schedule.from','=','from_name.matp')->leftjoin('vn_province as end_name','schedule.to','=','end_name.matp')->select('schedule.*','bus.license_plate as license_plate','drivers.name as driver_name','from_name.name as from_province','end_name.name as end_province')->get();
        return view('schedule.detail',['data'=>$data,'start_date'=>$request->start,'end_date'=>$request->end,'license_plate'=>$bus->license_plate,'id'=>$request->id]);
    }
    public function add_schedule(Request $request){
        $schedule = new Schedule;
        $schedule->bus = $request->bus;
        $schedule->driver = $request->driver;
        $schedule->from = $request->from;
        $schedule->to = $request->to;
        $schedule->start_time = $request->start_time;
        $schedule->end_time = $request->end_time;
        $schedule->save();
        echo "ok";
    }
    public function get_edit_schedule($id){
        $schedule = Schedule::find($id);
        echo json_encode($schedule);
    }
    public function post_edit_schedule(Request $request){
        $schedule = Schedule::find($request->id);
        $schedule->bus = $request->bus;
        $schedule->driver = $request->driver;
        $schedule->from = $request->from;
        $schedule->to = $request->to;
        $schedule->start_time = $request->start_time;
        $schedule->end_time = $request->end_time;
        $schedule->save();
        echo "ok";
    }
    public function delete_schedule($id){
        $schedule = Schedule::find($id);
        $schedule->delete();
        return back()->with('noti','Delete success');
    }
}
