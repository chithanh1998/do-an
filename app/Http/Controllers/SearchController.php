<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bus;
use App\Bus_type;
use App\Drivers;

class SearchController extends Controller
{
    //
    public function search(Request $request){
        $bus = Bus::where('license_plate','like', '%'.$request->search.'%')->get();
        $driver = Drivers::where('name','like', '%'.$request->search.'%')->get();
        $bus_type = Bus_type::all();
        return view('search',['bus'=>$bus, 'driver'=>$driver, 'bus_type'=>$bus_type, 'str'=>$request->search]);
    }
}
