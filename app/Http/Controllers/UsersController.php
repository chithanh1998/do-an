<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Str;
use App\Users;

class UsersController extends Controller
{
    //
    public function list(){
        $users = Users::all();
        return view('users.list',['user'=>$users]);
    }
    public function logout(){
        session()->flush();
        return redirect('/');
    }
    public function login(){
        $session = session()->get('role');
        if(!empty($session)){
             return redirect('dashboard');
        } else {
            return view('login');
        }
    }
    public function post_login(Request $request){
        $users = Users::where('username', $request->username)->where('password', md5($request->password))->get()->count();
        if($users == 1){
            $user = Users::where('username',$request->username)->first();
                
                // Chặn nhiều đăng nhập
                $last_session = \Session::getHandler()->read($user->last_sessid); // retrive last session
                if ($last_session) {
                    echo "err-login";
                } else {
                    session()->put('username', $request->username);
                    session()->put('name',$user->name);
                    session()->put('email',$user->email);
                    session()->put('avatar',$user->avatar);
                    session()->put('role',$user->role);
                    $new_sessid = \Session::getId();
                    $user->last_sessid = $new_sessid;
                    $user->save();
                    echo "ok";
                }
        } else {
            echo "err";
        }
    }
    public function post_add_user(Request $request){
        $users = Users::where('username', $request->username)->get()->count();
        if($users == 1){
            echo "err";
        } else {
        $users = new Users;
        $users->username = $request->username;
        $users->name = $request->name;
        $users->date_of_birth = $request->date_of_birth;
        $users->phone = $request->phone;
        $users->address = $request->address;
        $users->email = $request->email;
        $users->password = md5($request->password);
        $users->role = $request->role;
        $users->save();
        echo "ok";
        }
    }
    public function get_edit_user($id){
        $users = Users::find($id);
        echo json_encode($users);
    }
    public function post_edit_user(Request $request){
        $users = Users::find($request->id);
        if($request->password == ""){
            $users->name = $request->name;
            $users->email = $request->email;
            $users->date_of_birth = $request->date_of_birth;
            $users->role = $request->role;
            $users->phone = $request->phone;
            $users->address = $request->address;
            $users->save();
            echo "ok";
        } else {
            if(md5($request->password) == $users->password){
                $users->name = $request->name;
                $users->email = $request->email;
                $users->role = $request->role;
                $users->date_of_birth = $request->date_of_birth;
                $users->phone = $request->phone;
                $users->address = $request->address;
                $users->password = md5($request->new_password);
                $users->save();
                echo "ok";
            } else {
                echo "err";
            }
        }
    }
    public function get_delete_user($id){
        $users = Users::find($id);
        $users->delete();
        return back()->with('noti','Delete success');
    }
    public function get_detail_user($username){
        $user = Users::where('username', $username)->first();
        return view('users.detail',['user'=>$user]);
    }
    public function post_detail_user(Request $request, $username){
        $user = Users::where('username', $username)->first();
        $user->name = $request->fullname;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->date_of_birth = $request->date_of_birth;
        if($request->hasFile('avatar')){
            $file = $request->file('avatar');
            $crop = Image::make($file);
            $crop->fit(300);
            $name = $file->getClientOriginalName(); //Lấy tên file
            $avatar = Str::random(5)."_". $name;
            $crop->save(public_path()."/assets/images/users/".$avatar);
            $user->avatar = $avatar;
        }
        $user->save();

        session()->put('name',$user->name);
        session()->put('email',$user->email);
        session()->put('avatar',$user->avatar);
        return redirect('users/detail/'.$username)->with('noti','Save success');
    }
    public function post_pass_user(Request $request){
        $users = Users::where('username', $request->username)->where('password', md5($request->password))->get()->count();
        if($users == 0){
            echo "err";
        } else {
        $users = Users::where('username', $request->username)->first();
        $users->password = md5($request->new_password);
        $users->save();
        echo "ok";
        }
    }
}
