<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Blade::directive('datetime', function ($time) {
            return "<?php echo date_format(date_create($time),'Y-m-d H:i'); ?>";
        });
        Blade::directive('html_entity_decode', function ($str) {
            return "<?php echo html_entity_decode($str); ?>";
        });
    }
}
