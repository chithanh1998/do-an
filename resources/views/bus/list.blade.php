@extends('layout.main')
@section('css')
<link href="assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />

@endsection
@section('js')
<!-- Date Picker Plugin JavaScript -->
<script src="assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
@endsection
@section('content')
<!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Bus List</h3>
                        
                        </ol>
                    </div>

                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block">
                            @if(session('role') == 1)
                            <button style="margin-bottom:20px;" type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Add</button>
                            @endif
                            <table class="table" id="myTable">
                                        <thead>
                                            <tr>
                                                <th>License plate</th>
                                                <th>Type</th>
                                                <th>Import date</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($bus as $bu)
                                            <tr>
                                                <td>{{$bu->license_plate}}</td>
                                                @foreach($bus_type as $type)
                                                    @if($bu->type == $type->id)
                                                    <td>{{$type->type}}</td>
                                                    @endif
                                                @endforeach
                                                <td>{{$bu->import_date}}</td>
                                                @if($bu->status == 1)
                                                <td><span class="label label-success">Active</span></td>
                                                @endif
                                                @if($bu->status == 0)
                                                <td><span class="label label-danger">Maintenance</span></td>
                                                @endif
                                                <td class="text-nowrap">
                                                    <a href="schedule/detail/{{$bu->id}}"><span class="badge badge-pill badge-warning">Detail</span></a>
                                                    <a data-toggle="modal" class="edit_bus" data-target="#exampleModal1" data-whatever="@mdo" id="{{$bu->id}}"> <i style="margin-right:0px;" data-toggle="tooltip" data-original-title="Edit" data-animation="false" class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                                    @if(session('role') == 1)
                                                    <a href="bus/delete/{{$bu->id}}" onclick="javascript:confirmationDelete($(this));return false;" data-toggle="tooltip" data-original-title="Delete" data-animation="false"> <i class="fa fa-close text-danger connect" label="{{$bu->id}}"></i> </a>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- Thêm bus -->
                @if(session('role') == 1)
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel1">Add Bus</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <div class="modal-body">
                                            
                                                <form>
                                                <div class="alert alert-warning" style="display: none;" id="info"></div>
                                                <div class="form-group">
                                                        <label for="recipient-name" class="control-label">License plate</label>
                                                        <input type="text" class="form-control" id="license" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Type</label>
                                                        <select class="form-control p-0" style="width: 100%" name="type" id="type">
                                                        @foreach($bus_type as $type)
                                                        <option value="{{$type->id}}">{{$type->type}}</option>
                                                        @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Import date</label>
                                                        <div class="input-append date form_datetime">
                                                            <input id="import_date" class="form-control" type="text" name="time" value="" readonly>
                                                        <span class="add-on"><i class="icon-th"></i></span>
                                                    </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-info" id="add">Add</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                <!-- Thêm bus -->
                                <!-- Chỉnh sửa bus -->
                                <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel1">Edit</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <div class="modal-body">
                                            
                                                <form>
                                                <div class="alert alert-warning" style="display: none;" id="info"></div>
                                                <div class="form-group">
                                                        <label for="recipient-name" class="control-label">License plate</label>
                                                        <input type="text" class="form-control" id="license_edit" readonly>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Type</label>
                                                        <select class="form-control p-0" style="width: 100%" name="type" id="type_edit" @if(session('role') == 2) readonly @endif>
                                                        @foreach($bus_type as $type)
                                                        <option value="{{$type->id}}">{{$type->type}}</option>
                                                        @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Import date</label>
                                                        <div class="input-append date form_datetime">
                                                            <input id="import_date_edit" class="form-control" type="text" name="time" value="" readonly>
                                                        <span class="add-on"><i class="icon-th"></i></span>
                                                    </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Status</label>
                                                        <select class="form-control p-0" style="width: 100%" name="status" id="status_edit">
                                                        <option value="1">Active</option>
                                                        <option value="0">Maintenance</option>
                                                        </select>
                                                    </div>
                                                    <input type="hidden" name="bus_id" id="bus_id" />
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-info" id="save">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Chỉnh sửa bus -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
@endsection
@section('script')
<script>
$(document).ready(function() {
$('#myTable').DataTable(
            {
        
    }
        );
        @if(session('role') == 1)
        jQuery('.form_datetime').datepicker({
        toggleActive: false,
        format: 'yyyy-mm-dd',
        autoclose: true,
    });
    @endif
        @if(session('noti'))
    $.toast({
            heading: '{{session('noti')}}',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'success',
            hideAfter: 5000, 
            stack: 6,
            loader: false,
          });
    @endif
    @if(session('err'))
    $.toast({
            heading: '{{session('err')}}',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'warning',
            hideAfter: 5000, 
            stack: 6,
            loader: false,
          });
    @endif
    //Thêm bus
    $('#add').click(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var license = $('#license').val();
        var type = $('#type').val();
        var import_date = $('#import_date').val();
        $.ajax({
            type: 'post',
            url: 'bus/add',
            data: {
                license: license, type: type, import_date: import_date,
            },
            beforeSend: function(){
                
                if(license == "" || type == "" || import_date == ""){
                    $.toast({
                        heading: 'Error',
                        text: 'Please enter all fields',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'warning',
                        hideAfter: 5000, 
                        stack: 6,
                        loader: false,
                    });
                    return false;
                }
                $('.preloader').fadeIn();
            },
            success: function(resp){
                if(resp == "ok"){
                    $('.preloader').fadeOut();
                    $.toast({
                        heading: 'Add success',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'success',
                        hideAfter: 1500, 
                        stack: 6,
                        loader: false,
                    });
                    setTimeout('window.location.href = "bus";',1500);

                } else {
                    $('.preloader').fadeOut();
                    $.toast({
                        heading: 'Error',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'warning',
                        hideAfter: 5000, 
                        stack: 6,
                        loader: false,
                    });
                }
            }
        })
    })
    //Chỉnh sửa bus
    $("#myTable").on("click", ".edit_bus", function(){
// Lấy thông tin bus
        var id = $(this).attr("id");
        $.ajax({
            type: 'get',
            url: 'bus/edit/'+id,
            dataType: 'json',
            data:{
                id: id
            },
            success: function(resp){
                $('.preloader').fadeOut();
                $('#license_edit').val(resp.license_plate);
                $('#import_date_edit').val(resp.import_date);
                $('#type_edit').val(resp.type);
                $('#status_edit').val(resp.status);
                $('#bus_id').val(resp.id);
            }
        })
    })
    // Lưu thông tin chỉnh sửa


    $('#save').click(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var id = $('#bus_id').val();
        var license = $('#license_edit').val();
        var type = $('#type_edit').val();
        var import_date = $('#import_date_edit').val();
        var status = $('#status_edit').val();
        $.ajax({
            type: 'post',
            url: 'bus/edit/'+id,
            data: {
                id: id, license: license, type: type, import_date: import_date, status: status
            },
            beforeSend: function(){
                
                if(license == "" || type == "" || import_date == ""){
                    $.toast({
                        heading: 'Error',
                        text: 'Please enter all fields',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'warning',
                        hideAfter: 5000, 
                        stack: 6,
                        loader: false,
                    });
                    return false;
                }
                $('.preloader').fadeIn();
            },
            success: function(resp){
                $('.preloader').fadeOut();
                if(resp == "ok"){
                    
                    $.toast({
                        heading: 'Save success',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'success',
                        hideAfter: 1500, 
                        stack: 6,
                        loader: false,
                    });
                    setTimeout('window.location.href = "bus";',1500);

                } else {
                    $.toast({
                        heading: 'Error',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'warning',
                        hideAfter: 5000, 
                        stack: 6,
                        loader: false,
                    });
                }
            }
        })
    })
})
function confirmationDelete(anchor) {
    var conf = swal({   
        title: "Are you sure?",      
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Yes",
        cancelButtonText: "No",   
        closeOnConfirm: false 
    }, function(){   
        window.location = anchor.attr("href"); 
    });
    
    
}
function formatNumber(num) {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}
</script>
@endsection