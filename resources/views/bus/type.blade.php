@extends('layout.main')
@section('content')
<!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Bus type list</h3>
                        
                        </ol>
                    </div>

                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block">
                            @if(session('role') == 1)
                            <button style="margin-bottom:20px;" type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Add</button>
                            @endif
                            <table class="table" id="myTable">
                                        <thead>
                                            <tr>
                                                <th>Type</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($bus_type as $type)
                                            <tr>
                                                <td>{{$type->type}}</td>
                                                <td class="text-nowrap">
                                                @if(session('role') == 1)
                                                    <a href="bus/type/delete/{{$type->id}}" onclick="javascript:confirmationDelete($(this));return false;" data-toggle="tooltip" data-original-title="Delete" data-animation="false"> <i class="fa fa-close text-danger connect" label="{{$type->id}}"></i> </a>
                                                @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- Thêm loại bus -->
                @if(session('role') == 1)
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel1">Add Bus type</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <div class="modal-body">
                                            
                                                <form>
                                                <div class="alert alert-warning" style="display: none;" id="info"></div>
                                                <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Type</label>
                                                        <input type="text" class="form-control" id="type" required>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-info" id="add">Add</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Thêm loại bus -->
                @endif
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
@endsection
@section('script')
<script>
$(document).ready(function() {
$('#myTable').DataTable(
            {
        
    }
        );
        @if(session('noti'))
    $.toast({
            heading: '{{session('noti')}}',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'success',
            hideAfter: 5000, 
            stack: 6,
            loader: false,
          });
    @endif
    @if(session('err'))
    $.toast({
            heading: '{{session('err')}}',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'warning',
            hideAfter: 5000, 
            stack: 6,
            loader: false,
          });
    @endif
    //Thêm loai bus
    $('#add').click(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var type = $('#type').val();
        $.ajax({
            type: 'post',
            url: 'bus/type/add',
            data: {
                type: type,
            },
            beforeSend: function(){  
                if(type == ""){
                    $.toast({
                        heading: 'Error',
                        text: 'Please enter type',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'warning',
                        hideAfter: 5000, 
                        stack: 6,
                        loader: false,
                    });
                    return false;
                }
                $('.preloader').fadeIn();
            },
            success: function(resp){
                if(resp == "ok"){
                    $('.preloader').fadeOut();
                    $.toast({
                        heading: 'Add success',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'success',
                        hideAfter: 1500, 
                        stack: 6,
                        loader: false,
                    });
                    setTimeout('window.location.href = "bus/type";',1500);

                } else {
                    $('.preloader').fadeOut();
                    $.toast({
                        heading: 'Error',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'warning',
                        hideAfter: 5000, 
                        stack: 6,
                        loader: false,
                    });
                }
            }
        })
    })
})
function confirmationDelete(anchor) {
    var conf = swal({   
        title: "Are you sure?",      
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Yes",
        cancelButtonText: "No",   
        closeOnConfirm: false 
    }, function(){   
        window.location = anchor.attr("href"); 
    });
    
    
}
function formatNumber(num) {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}
</script>
@endsection