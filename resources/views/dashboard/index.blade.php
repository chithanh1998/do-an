@extends('layout.main')
@section('css')
<link href="assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="https://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
@endsection
@section('js')
<!-- Date Picker Plugin JavaScript -->
<script src="assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
    <!--- New Date module -->
@endsection
@section('content')
<!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        
                        </ol>
                    </div>
                    <div class="col-md-6 col-4 align-self-center">
                    
                    <form class="input-daterange input-group col-md-6 pull-right m-r-10" id="year-range" style="display: none" method="GET" action="dashboard/year/">
                
                        <input type="text" class="span2 form-control" id="start-date" name="start" autocomplete="off" required>
                        <span class="input-group-addon bg-info b-0 text-white">To</span>
                        <input type="text" class="span2 form-control" id="end-date" name="end" autocomplete="off" required>
                        <button type="submit" class="btn btn-success m-l-20" id="date-ok">OK</button>
                    </form>
                        <div class="dropdown pull-right m-r-40 hidden-sm-down">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @if($data['filter'] == 'month') Month @elseif($data['filter'] == 'all') All time @else {{$data['filter']}} @endif 
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="dashboard/">Month</a>
                                <a class="dropdown-item" id="year-pick">Year</a>
                                <a class="dropdown-item" href="dashboard/all">All</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-md-6 col-lg-4 col-xlg-4">
                        <div class="card card-primary card-inverse">
                            <div class="box text-center">
                                <h1 class="font-light text-white">{{$data['total_bus']}}</h1>
                                <h6 class="text-white">Total Bus</h6>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-md-6 col-lg-4 col-xlg-4">
                        <div class="card card-inverse card-success">
                            <div class="box text-center">
                                <h1 class="font-light text-white">{{$data['total_drivers']}}</h1>
                                <h6 class="text-white">Total Drivers</h6>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-md-6 col-lg-4 col-xlg-4">
                        <div class="card card-inverse card-warning">
                            <div class="box text-center">
                                <h1 class="font-light text-white">{{$data['total_schedule']}}</h1>
                                <h6 class="text-white">Total Buses</h6>
                            </div>
                        </div>
                    </div>
                </div>
                @if($data['filter'] != 'all')
                <div class="row">

                            <div class="col-lg-6">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">Tổng số chuyến</h4>
                                <div id="morris-bar-chart-schedule"></div>
                            </div>
                        </div>
                    </div>
                @endif
                    <!-- <div class="col-lg-6">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">Tổng số tiền nhận được</h4>
                                <div id="morris-bar-chart-betmoney"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">Tổng số trận</h4>
                                <div id="morris-bar-chart-match"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">Tổng số cược thắng/thua</h4>
                                <div id="morris-bar-chart-betwl"></div>
                            </div>
                        </div>
                    </div> -->
                    @if($data['filter'] != 'all')
                    <div class="col-lg-6">
                    @else
                    <div class="col-lg-12">
                    @endif
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">Tỉ lệ loại xe sử dụng</h4>
                                <div class="flot-chart">
                                    <div class="flot-chart-content" id="flot-pie-chart"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">Tuyến đường đi nhiều nhất</h4>
                                <div class="table-responsive">
                                    <table class="table color-bordered-table success-bordered-table">
                                        <thead>
                                            <tr>
                                                <th>Tuyến đường</th>
                                                <th>Số lần</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($rank_td as $td)
                                            <tr>
                                                <td>{{$td->from}} - {{$td->to}}</td>
                                                <td>{{$td->total}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">Xe được dùng nhiều nhất</h4>
                                <div class="table-responsive">
                                    <table class="table color-bordered-table info-bordered-table">
                                        <thead>
                                            <tr>
                                                <th>Xe</th>
                                                <th>Số lần</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($rank_bus as $rb)
                                            <tr>
                                                <td>{{$rb->license_plate}}</td>
                                                <td>{{$rb->total}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
               
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

@endsection
@section('script')
<script>
$(function () {
    var data = @php echo json_encode($data['ratio_type']); @endphp;
    var plotObj = $.plot($('#flot-pie-chart'), data, {
        series: {
            pie: {
                innerRadius: 0.5
                , show: true
            }
        }
        , grid: {
            hoverable: true
        }
        , color: null
        , tooltip: true
        , tooltipOpts: {
            content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
            shifts: {
                x: 20
                , y: 0
            }
            , defaultTheme: false
        }
    });
});
$(document).ready(function(){
    jQuery('#year-range').datepicker({
        toggleActive: false,
        format: 'yyyy',
        autoclose: true,
        minViewMode: 2,
    });
    // Biểu đồ số chuyến
    @if($data['filter'] != 'all')
    Morris.Bar({
        element: 'morris-bar-chart-schedule',
        data: @if(!empty($data['schedule_month'])) @html_entity_decode($data['schedule_month']) @else @php echo json_encode($data['schedule_year']); @endphp @endif,
        xkey: 'y',
        ykeys: ['a'],
        labels: ['Số chuyến'],
        barColors:['#55ce63'],
        hideHover: 'auto',
        gridLineColor: '#eef0f2',
        resize: true
    });
    @endif
    $('#year-pick').click(function(){
        $('#year-range').fadeIn();
    });
})
</script>
<script>
$(document).ready(function(){
  $("#start-date").change(function(){
        $("#end-date").focus();
  });
});
</script>
@endsection
@section('js')

@endsection