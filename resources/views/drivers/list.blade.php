@extends('layout.main')
@section('css')
<link href="assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />

@endsection
@section('js')
<!-- Date Picker Plugin JavaScript -->
<script src="assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
@endsection
@section('content')
<!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Driver List</h3>
                        
                        </ol>
                    </div>

                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block">
                            @if(session('role') == 1)
                            <button style="margin-bottom:20px;" type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Add</button>
                            @endif
                            <table class="table" id="myTable">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Date of birth</th>
                                                <th>Phone</th>
                                                <th>Address</th>
                                                <th>Status</th>
                                                <th>Action</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($driver as $dr)
                                            <tr>
                                                <td>{{$dr->name}}</td>
                                                <td>{{$dr->date_of_birth}}</td>
                                                <td>{{$dr->phone}}</td>
                                                <td>{{$dr->address}}</td>
                                                @if($dr->status == 1)
                                                <td><span class="label label-success">Work</span></td>
                                                @endif
                                                @if($dr->status == 0)
                                                <td><span class="label label-danger">Busy</span></td>
                                                @endif
                                                <td class="text-nowrap">
                                                    <a data-toggle="modal" class="edit_driver" data-target="#exampleModal1" data-whatever="@mdo" id="{{$dr->id}}"> <i style="margin-right:0px;" data-toggle="tooltip" data-original-title="Edit" data-animation="false" class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                                    @if(session('role') == 1)
                                                    <a href="drivers/delete/{{$dr->id}}" onclick="javascript:confirmationDelete($(this));return false;" data-toggle="tooltip" data-original-title="Delete" data-animation="false"> <i class="fa fa-close text-danger connect" label="{{$dr->id}}"></i> </a>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- Thêm lái xe -->
                @if(session('role') == 1)
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel1">Add Driver</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <div class="modal-body">
                                            
                                                <form>
                                                <div class="alert alert-warning" style="display: none;" id="info"></div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Name</label>
                                                        <input type="text" class="form-control" id="name" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Date of birth</label>
                                                        <div class="input-append date form_datetime">
                                                            <input id="date_of_birth" class="form-control" type="text" name="time" value="" readonly>
                                                        <span class="add-on"><i class="icon-th"></i></span>
                                                    </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Phone</label>
                                                        <input type="number" class="form-control" id="phone" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Address</label>
                                                        <input type="text" class="form-control" id="address" required>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-info" id="add">Add</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                <!-- Thêm lái xe -->
                                <!-- Chỉnh sửa lái xe -->
                                <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel1">Edit</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <div class="modal-body">
                                            
                                                <form>
                                                <div class="alert alert-warning" style="display: none;" id="info"></div>

                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Name</label>
                                                        <input type="text" class="form-control" id="name_edit" required @if(session('role') == 2) readonly @endif>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Date of birth</label>
                                                        <div class="input-append date form_datetime">
                                                            <input id="date_of_birth_edit" class="form-control" type="text" name="time" value="" readonly>
                                                        <span class="add-on"><i class="icon-th"></i></span>
                                                    </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Phone</label>
                                                        <input type="number" class="form-control" id="phone_edit" required @if(session('role') == 2) readonly @endif>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Address</label>
                                                        <input type="text" class="form-control" id="address_edit" required @if(session('role') == 2) readonly @endif>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Status</label>
                                                        <select class="form-control p-0" style="width: 100%" name="status" id="status_edit">
                                                        <option value="1">Work</option>
                                                        <option value="0">Busy</option>
                                                        </select>
                                                    </div>
                                                    <input type="hidden" name="driver_id" id="driver_id" />
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-info" id="save">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Chỉnh sửa lái xe -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
@endsection
@section('script')
<script>
$(document).ready(function() {
$('#myTable').DataTable(
            {
        
    }
        );
        @if(session('role') == 1)
        jQuery('.form_datetime').datepicker({
        toggleActive: false,
        format: 'yyyy-mm-dd',
        startView: 2,
        autoclose: true,
    });
    @endif
        @if(session('noti'))
    $.toast({
            heading: '{{session('noti')}}',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'success',
            hideAfter: 5000, 
            stack: 6,
            loader: false,
          });
    @endif
    //Thêm lái xe
    $('#add').click(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var name = $('#name').val();
        var birth = $('#date_of_birth').val();
        var phone = $('#phone').val();
        var address = $('#address').val();
        $.ajax({
            type: 'post',
            url: 'drivers/add',
            data: {
                name: name, phone: phone, address: address, date_of_birth: birth
            },
            beforeSend: function(){
                
                if(name == "" || phone == "" || address == "" || birth == ""){
                    $.toast({
                        heading: 'Error',
                        text: 'Please enter all fields',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'warning',
                        hideAfter: 5000, 
                        stack: 6,
                        loader: false,
                    });
                    return false;
                }
                $('.preloader').fadeIn();
            },
            success: function(resp){
                if(resp == "ok"){
                    $('.preloader').fadeOut();
                    $.toast({
                        heading: 'Add success',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'success',
                        hideAfter: 1500, 
                        stack: 6,
                        loader: false,
                    });
                    setTimeout('window.location.href = "drivers";',1500);

                } else {
                    $('.preloader').fadeOut();
                    $.toast({
                        heading: 'Error',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'warning',
                        hideAfter: 5000, 
                        stack: 6,
                        loader: false,
                    });
                }
            }
        })
    })
    //Chỉnh sửa lái xe
    $("#myTable").on("click", ".edit_driver", function(){
// Lấy thông tin lãi xe
        var id = $(this).attr("id");
        $.ajax({
            type: 'get',
            url: 'drivers/edit/'+id,
            dataType: 'json',
            data:{
                id: id
            },
            success: function(resp){
                $('.preloader').fadeOut();
                $('#name_edit').val(resp.name);
                $('#date_of_birth_edit').val(resp.date_of_birth);
                $('#phone_edit').val(resp.phone);
                $('#address_edit').val(resp.address);
                $('#status_edit').val(resp.status);
                $('#driver_id').val(resp.id);
            }
        })
    })
    // Lưu thông tin chỉnh sửa


    $('#save').click(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var id = $('#driver_id').val();
        var name = $('#name_edit').val();
        var birth = $('#date_of_birth_edit').val();
        var phone = $('#phone_edit').val();
        var address = $('#address_edit').val();
        var status = $('#status_edit').val();
        $.ajax({
            type: 'post',
            url: 'drivers/edit/'+id,
            data: {
                id: id, name: name, phone: phone, address: address, date_of_birth: birth, status: status
            },
            beforeSend: function(){
                
                if(name == "" || phone == "" || address == "" || birth == ""){
                    $.toast({
                        heading: 'Error',
                        text: 'Please enter all fields',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'warning',
                        hideAfter: 5000, 
                        stack: 6,
                        loader: false,
                    });
                    return false;
                }
                $('.preloader').fadeIn();
            },
            success: function(resp){
                $('.preloader').fadeOut();
                if(resp == "ok"){
                    
                    $.toast({
                        heading: 'Save success',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'success',
                        hideAfter: 1500, 
                        stack: 6,
                        loader: false,
                    });
                    setTimeout('window.location.href = "drivers";',1500);

                } else {
                    $.toast({
                        heading: 'Error',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'warning',
                        hideAfter: 5000, 
                        stack: 6,
                        loader: false,
                    });
                }
            }
        })
    })
})
function confirmationDelete(anchor) {
    var conf = swal({   
        title: "Are you sure?",      
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Yes",
        cancelButtonText: "No",   
        closeOnConfirm: false 
    }, function(){   
        window.location = anchor.attr("href"); 
    });
    
    
}
function formatNumber(num) {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}
</script>
@endsection