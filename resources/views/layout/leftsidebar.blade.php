<!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-small-cap">Menu</li>
                        <li>
                            <a href="dashboard" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard</span></a>
                        </li>
                        @if(session('role') == 1)
                        <li>
                            <a href="users" aria-expanded="false"><i class="mdi mdi-account"></i><span class="hide-menu">Users</span></a>
                        </li>
                        @endif  
                        <li>
                            <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-bus"></i><span class="hide-menu">Bus</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="bus">List</a></li>
                                <li><a href="bus/type">Type</a></li>
                            </ul>
                        </li>  
                        <li>
                            <a href="drivers" aria-expanded="false"><i class="mdi mdi-compass"></i><span class="hide-menu">Drivers</span></a>
                        </li>
                        <li>
                            <a href="schedule" aria-expanded="false"><i class="mdi mdi-timetable"></i><span class="hide-menu">Schedule</span></a>
                        </li> 
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
           <!-- Bottom points-->
           <div class="sidebar-footer">
                <!-- item-->
                <!-- <a href="admin/settings" class="link" data-toggle="tooltip" title="Cài đặt"><i class="ti-settings"></i></a> -->
                
            </div>
            <!-- End Bottom points-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->