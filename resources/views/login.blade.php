<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>Login</title>
    <!-- Bootstrap Core CSS -->
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
     <!-- toast CSS -->
     <link href="assets/plugins/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="css/colors/blue.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper" class="login-register login-sidebar"  style="background-image:url(assets/images/background/login.jpg);">
  <div class="login-box card">
    <div class="card-block">
      <form class="form-horizontal form-material" id="loginform">
        <a href="javascript:void(0)" class="text-center db"><img src="assets/images/logo.png" alt="Home" /><br/></a>  
        
        <div class="form-group m-t-40">
          <div class="col-xs-12">
            <input class="form-control" id="username" type="text" placeholder="Username">
          </div>
        </div>
        <div class="form-group">
          <div class="col-xs-12">
            <input class="form-control" id="password" type="password" placeholder="Password">
          </div>
        </div>
        <!-- <div class="form-group">
          <div class="col-md-12">
            <div class="checkbox checkbox-primary pull-left p-t-0">
              <input id="checkbox-signup" type="checkbox">
              <label for="checkbox-signup"> Remember me </label>
            </div>
            <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> Forgot pwd?</a> </div>
        </div> -->
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="button" id="login">Log In</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/plugins/bootstrap/js/tether.min.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="js/custom.min.js"></script>
    <script src="assets/plugins/toast-master/js/jquery.toast.js"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
</body>
<script>
    $(document).ready(function(){
      @if(session('noti'))
    $.toast({
            heading: '{{session('noti')}}',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 5000, 
            stack: 6,
            loader: false,
          });
    @endif
        // Enter đăng nhập
        $(document).bind('keydown', function(e){         
    if (e.which == 13){
       $('#login').trigger('click');   
   }     
      });
        $('#login').click(function(){
            var username = $('#username').val();
            var password = $('#password').val();
            $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: 'login',
            data: {'username':username, 'password':password},
            beforeSend: function(){
                if(username == "" || password == ""){
                    $.toast({
                        heading: 'Error',
                        text: 'Please enter username and password',
                        position: 'top-right',
                        loaderBg:'#eb4034',
                        icon: 'error',
                        hideAfter: 5000, 
                        stack: 1,
                        loader: false,
                    });
                    return false;
                }
                $('.preloader').fadeIn();
            },
            success: function(resp){
                if(resp == "ok"){
                    setTimeout('window.location.href = "dashboard";',1000);
                }
                if(resp == "err"){
                    $('.preloader').fadeOut();
                    $.toast({
                        heading: 'Error',
                        text: 'Incorrect username or password',
                        position: 'top-right',
                        loaderBg:'#eb4034',
                        icon: 'error',
                        hideAfter: 5000, 
                        stack: 1,
                        loader: false,
                    });
                }
                if(resp == "err-login"){
                    $('.preloader').fadeOut();
                    $.toast({
                        heading: 'Error',
                        text: 'Account in use',
                        position: 'top-right',
                        loaderBg:'#eb4034',
                        icon: 'error',
                        hideAfter: 5000, 
                        stack: 1,
                        loader: false,
                    });
                }
                    } 
        })
        })
    })
</script>
</html>