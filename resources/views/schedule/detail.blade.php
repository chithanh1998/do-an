@extends('layout.main')
@section('css')
<link href="assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
@endsection
@section('js')
<!-- Date Picker Plugin JavaScript -->
<script src="assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

@endsection
@section('content')
<!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Schedule Detail {{$license_plate}}</h3>
                        
                        </ol>
                    </div>

                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block">
                            <form class="input-daterange input-group col-md-4" style="display: inline-flex; margin-left: 300px; margin-bottom: 10px; padding-bottom: 10px" id="date-range" method="GET" action="schedule/detail/{{$id}}/date">
                    
                    <input type="text" class="form-control" autocomplete="off" id="start-date" value="{{$start_date}}" name="start" required>
                    <span class="input-group-addon bg-info b-0 text-white">To</span>
                    <input type="text" class="form-control" autocomplete="off" id="end-date" name="end" value="{{$end_date}}" required>

                <button type="submit" class="btn btn-success m-l-20" id="get_page">Show</button></form>
                            <table class="table" id="myTable">
                                        <thead>
                                            <tr>
                                                <th>Driver</th>
                                                <th>From</th>
                                                <th>To</th>
                                                <th>Start time</th>
                                                <th>End time</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($data as $dt)
                                            <tr>
                                                <td>{{$dt->driver_name}}</td>
                                                <td>{{$dt->from_province}}</td>
                                                <td>{{$dt->end_province}}</td>
                                                <td>@datetime($dt->start_time)</td>
                                                <td>@datetime($dt->end_time)</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
@endsection
@section('script')
<script>
$(document).ready(function() {
$('#myTable').DataTable(
            {
               "order": [[ 3, "desc" ]],
    }
        );
        jQuery('.input-daterange').datepicker({
        toggleActive: false,
        format: 'yyyy-mm-dd',
    });
    $(".form_datetime").datetimepicker({
        format: "yyyy-mm-dd hh:ii",
        autoclose: true,
        pickerPosition: 'top-right'
    });
    $('.select2').select2({
        dropdownParent: $('#exampleModal'),
    });
    $('.select2_edit').select2({
        dropdownParent: $('#exampleModal1'),
    });
        @if(session('noti'))
    $.toast({
            heading: '{{session('noti')}}',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'success',
            hideAfter: 5000, 
            stack: 6,
            loader: false,
          });
    @endif
})
function confirmationDelete(anchor) {
    var conf = swal({   
        title: "Are you sure?",      
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Yes",
        cancelButtonText: "No",   
        closeOnConfirm: false 
    }, function(){   
        window.location = anchor.attr("href"); 
    });
    
    
}
function formatNumber(num) {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}
</script>
@endsection