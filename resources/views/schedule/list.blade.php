@extends('layout.main')
@section('css')
<link href="assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
@endsection
@section('js')
<!-- Date Picker Plugin JavaScript -->
<script src="assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

@endsection
@section('content')
<!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Schedule List</h3>
                        
                        </ol>
                    </div>

                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block">
                            <button style="margin-bottom:20px; margin-top: 10px" type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Add</button>
                            <form class="input-daterange input-group col-md-4" style="display: inline-flex; margin-left: 300px; margin-bottom: 10px; padding-bottom: 10px" id="date-range" method="GET" action="schedule/date">
                    
                    <input type="text" class="form-control" autocomplete="off" id="start-date" value="{{$start_date}}" name="start" required>
                    <span class="input-group-addon bg-info b-0 text-white">To</span>
                    <input type="text" class="form-control" autocomplete="off" id="end-date" name="end" value="{{$end_date}}" required>

                <button type="submit" class="btn btn-success m-l-20" id="get_page">Show</button></form>
                            <table class="table" id="myTable">
                                        <thead>
                                            <tr>
                                                <th>Bus</th>
                                                <th>Driver</th>
                                                <th>From</th>
                                                <th>To</th>
                                                <th>Start time</th>
                                                <th>End time</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($data as $dt)
                                            <tr>
                                                <td><a href="schedule/detail/{{$dt->bus}}">{{$dt->license_plate}}</a></td>
                                                <td>{{$dt->driver_name}}</td>
                                                <td>{{$dt->from_province}}</td>
                                                <td>{{$dt->end_province}}</td>
                                                <td>@datetime($dt->start_time)</td>
                                                <td>@if(!empty($dt->end_time)) @datetime($dt->end_time) @endif</td>
                                                <td class="text-nowrap">
                                                    <a data-toggle="modal" class="edit_schedule" data-target="#exampleModal1" data-whatever="@mdo" id="{{$dt->id}}"> <i style="margin-right:0px;" data-toggle="tooltip" data-original-title="Edit" data-animation="false" class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                                    <a href="schedule/delete/{{$dt->id}}" onclick="javascript:confirmationDelete($(this));return false;" data-toggle="tooltip" data-original-title="Delete" data-animation="false"> <i class="fa fa-close text-danger connect" label="{{$dt->id}}"></i> </a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- Thêm lịch trình -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel1">Add Schedule</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <div class="modal-body">
                                            
                                                <form>
                                                <div class="alert alert-warning" style="display: none;" id="info"></div>
                                                <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Bus</label>
                                                        <select class="form-control p-0 select2" style="width: 100%" name="bus" id="bus">
                                                        @foreach($bus as $bu)
                                                        <option value="{{$bu->id}}" @if($bu->status == 0) disabled @endif>{{$bu->license_plate}} @foreach($bus_type as $type) @if($bu->type == $type->id)  ({{$type->type}}) @endif @endforeach</option>
                                                        @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Driver</label>
                                                        <select class="form-control p-0 select2" style="width: 100%" name="driver" id="driver">
                                                        @foreach($driver as $dr)
                                                        <option value="{{$dr->id}}" @if($dr->status == 0) disabled @endif>{{$dr->name}}</option>
                                                        @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">From</label>
                                                        <select class="form-control p-0 select2" style="width: 100%" name="from" id="from">
                                                        @foreach($province as $pr)
                                                        <option value="{{$pr->matp}}">{{$pr->name}}</option>
                                                        @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">To</label>
                                                        <select class="form-control p-0 select2" style="width: 100%" name="to" id="to">
                                                        @foreach($province as $pr)
                                                        <option value="{{$pr->matp}}">{{$pr->name}}</option>
                                                        @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Start time</label>
                                                        <div class="input-append date form_datetime">
                                                            <input id="start_time" class="form-control" type="text" name="time" value="" readonly>
                                                        <span class="add-on"><i class="icon-th"></i></span>
                                                    </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">End time</label>
                                                        <div class="input-append date form_datetime">
                                                            <input id="end_time" class="form-control" type="text" autocomplete="off" name="time" value="">
                                                        <span class="add-on"><i class="icon-th"></i></span>
                                                    </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-info" id="add">Add</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Thêm lich trinh -->
                                <!-- Chỉnh sửa lich trinh -->
                                <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel1">Edit</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <div class="modal-body">
                                            
                                                <form>
                                                <div class="alert alert-warning" style="display: none;" id="info"></div>
                                                <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Bus</label>
                                                        <select class="form-control p-0 select2_edit" style="width: 100%" name="bus_edit" id="bus_edit" disabled>
                                                        @foreach($bus as $bu)
                                                        <option value="{{$bu->id}}" @if($bu->status == 0) disabled @endif>{{$bu->license_plate}}</option>
                                                        @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Driver</label>
                                                        <select class="form-control p-0 select2_edit" style="width: 100%" name="driver" id="driver_edit">
                                                        @foreach($driver as $dr)
                                                        <option value="{{$dr->id}}" @if($dr->status == 0) disabled @endif>{{$dr->name}}</option>
                                                        @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">From</label>
                                                        <select class="form-control p-0 select2_edit" style="width: 100%" name="from_edit" id="from_edit">
                                                        @foreach($province as $pr)
                                                        <option value="{{$pr->matp}}">{{$pr->name}}</option>
                                                        @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">To</label>
                                                        <select class="form-control p-0 select2_edit" style="width: 100%" name="to_edit" id="to_edit">
                                                        @foreach($province as $pr)
                                                        <option value="{{$pr->matp}}">{{$pr->name}}</option>
                                                        @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Start time</label>
                                                        <div class="input-append date form_datetime">
                                                            <input id="start_time_edit" class="form-control" type="text" name="time" value="" readonly>
                                                        <span class="add-on"><i class="icon-th"></i></span>
                                                    </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">End time</label>
                                                        <div class="input-append date form_datetime">
                                                            <input id="end_time_edit" class="form-control" type="text" autocomplete="off" name="time" value="">
                                                        <span class="add-on"><i class="icon-th"></i></span>
                                                    </div>
                                                    </div>
                                                    <input type="hidden" name="schedule_id" id="schedule_id" />
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-info" id="save">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Chỉnh sửa lichh trinh -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
@endsection
@section('script')
<script>
$(document).ready(function() {
$('#myTable').DataTable(
            {
               "order": [[ 4, "desc" ]],
    }
        );
        jQuery('.input-daterange').datepicker({
        toggleActive: false,
        format: 'yyyy-mm-dd',
        autoclose: true,
    });
    $(".form_datetime").datetimepicker({
        format: "yyyy-mm-dd hh:ii",
        autoclose: true,
        pickerPosition: 'top-right'
    });
    $('.select2').select2({
        dropdownParent: $('#exampleModal'),
    });
    $('.select2_edit').select2({
        dropdownParent: $('#exampleModal1'),
    });
        @if(session('noti'))
    $.toast({
            heading: '{{session('noti')}}',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'success',
            hideAfter: 5000, 
            stack: 6,
            loader: false,
          });
    @endif
    //Thêm lich trinh
    $('#add').click(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var bus = $('#bus').val();
        var driver = $('#driver').val();
        var from = $('#from').val();
        var to = $('#to').val();
        var start_time = $('#start_time').val();
        var end_time = $('#end_time').val();
        var start_date = $('#start-date').val();
        var end_date = $('#end-date').val();
        console.log(from,to);
        $.ajax({
            type: 'post',
            url: 'schedule/add',
            data: {
                bus: bus, driver: driver, from: from, to: to, start_time: start_time, end_time: end_time
            },
            beforeSend: function(){
                if(from == to){
                    $.toast({
                        heading: 'Error',
                        text: 'Duplicate location',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'warning',
                        hideAfter: 5000, 
                        stack: 6,
                        loader: false,
                    });
                    return false;
                }
                if(start_time == ""){
                    $.toast({
                        heading: 'Error',
                        text: 'Please enter start time',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'warning',
                        hideAfter: 5000, 
                        stack: 6,
                        loader: false,
                    });
                    return false;
                }
                $('.preloader').fadeIn();
            },
            success: function(resp){
                if(resp == "ok"){
                    $('.preloader').fadeOut();
                    $.toast({
                        heading: 'Add success',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'success',
                        hideAfter: 1500, 
                        stack: 6,
                        loader: false,
                    });
                    setTimeout('window.location.href = "schedule/date?start='+start_date+'&end='+end_date+'";',1500);

                } else {
                    $('.preloader').fadeOut();
                    $.toast({
                        heading: 'Error',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'warning',
                        hideAfter: 5000, 
                        stack: 6,
                        loader: false,
                    });
                }
            }
        })
    })
    //Chỉnh sửa lich trinh
    $("#myTable").on("click", ".edit_schedule", function(){
// Lấy thông tin lich trinh
        var id = $(this).attr("id");
        $.ajax({
            type: 'get',
            url: 'schedule/edit/'+id,
            dataType: 'json',
            data:{
                id: id
            },
            success: function(resp){
                $('.preloader').fadeOut();
                console.log(resp.from,resp.to);
                $('#bus_edit').val(resp.bus).trigger('change');
                $('#driver_edit').val(resp.driver).trigger('change');
                $('#from_edit').val(resp.from).trigger('change');
                $('#to_edit').val(resp.to).trigger('change');
                $('#start_time_edit').val(resp.start_time);
                $('#end_time_edit').val(resp.end_time);
                $('#schedule_id').val(resp.id);
            }
        })
    })
    // Lưu thông tin chỉnh sửa


    $('#save').click(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var id = $('#schedule_id').val();
        var bus = $('#bus_edit').val();
        var driver = $('#driver_edit').val();
        var from_location = $('#from_edit').val();
        var to_location = $('#to_edit').val();
        var start_time = $('#start_time_edit').val();
        var end_time = $('#end_time_edit').val();
        var start_date = $('#start-date').val();
        var end_date = $('#end-date').val();
        $.ajax({
            type: 'post',
            url: 'schedule/edit/'+id,
            data: {
                id: id, bus: bus, driver: driver, from: from_location, to: to_location, start_time: start_time, end_time: end_time
            },
            beforeSend: function(){
                
                if(start_time == ""){
                    $.toast({
                        heading: 'Error',
                        text: 'Please enter start time',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'warning',
                        hideAfter: 5000, 
                        stack: 6,
                        loader: false,
                    });
                    return false;
                }
                console.log(from_location);
                $('.preloader').fadeIn();
            },
            success: function(resp){
                $('.preloader').fadeOut();
                if(resp == "ok"){
                    
                    $.toast({
                        heading: 'Save success',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'success',
                        hideAfter: 1500, 
                        stack: 6,
                        loader: false,
                    });
                    setTimeout('window.location.href = "schedule/date?start='+start_date+'&end='+end_date+'";',1500);

                } else {
                    $.toast({
                        heading: 'Error',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'warning',
                        hideAfter: 5000, 
                        stack: 6,
                        loader: false,
                    });
                }
            }
        })
    })
})
function confirmationDelete(anchor) {
    var conf = swal({   
        title: "Are you sure?",      
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Yes",
        cancelButtonText: "No",   
        closeOnConfirm: false 
    }, function(){   
        window.location = anchor.attr("href"); 
    });
    
    
}
function formatNumber(num) {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}
</script>
@endsection