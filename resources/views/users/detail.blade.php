@extends('layout.main')
@section('css')
<link href="assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />

@endsection
@section('js')
<!-- Date Picker Plugin JavaScript -->
<script src="assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
@endsection
@section('content')
<div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-block">
                                <center class="m-t-30"> <img src="assets/images/users/{{$user->avatar}}" class="img-circle" width="150" />
                                    <h4 class="card-title m-t-10">{{$user->name}}</h4>
                                    
                                </center>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist">
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#settings" role="tab">Settings</a> </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                
                                <div class="tab-pane active" id="settings" role="tabpanel">
                                    <div class="card-block">
                                        <form class="form-horizontal form-material" action="users/detail/{{$user->username}}" method="post" enctype="multipart/form-data">
                                        @csrf
                                            <div class="form-group">
                                    
                                                
                                                <label class="col-md-12">Name</label>
                                                <div class="col-md-12">
                                                    <input type="text" value="{{$user->name}}" name="fullname" class="form-control form-control-line" require>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="example-email" class="col-md-12">Email</label>
                                                <div class="col-md-12">
                                                    <input type="email" value="{{$user->email}}" name="email" class="form-control form-control-line" name="example-email" id="example-email" require>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                            <label class="col-md-12">Phone</label>
                                                <div class="col-md-12">
                                                    <input type="number" value="{{$user->phone}}" name="phone" class="form-control form-control-line" require>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                            <label class="col-md-12">Address</label>
                                                <div class="col-md-12">
                                                    <input type="text" value="{{$user->address}}" name="address" class="form-control form-control-line" require>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                            <label class="col-md-12">Date Of Birth</label>
                                                <div class="col-md-12">
                                                    <input type="text" value="{{$user->date_of_birth}}" id="date_birth" name="date_of_birth" class="form-control form-control-line" require>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                        <label class="col-md-12">Avatar</label>
                                        <div class="fileinput fileinput-new input-group col-md-12" data-provides="fileinput">
                                            <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Upload</span> <span class="fileinput-exists">Other</span>
                                            <input type="file" name="avatar"> </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Delete</a> </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <button type="submit" class="btn btn-success">Save</button>
                                                </div>
                                            </div>
                                            <button style="margin: 0px 0px 15px 15px;" type="button" class="btn btn-info" data-toggle="modal" data-target="#passModal" data-whatever="@mdo">Change password</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
<!-- Đổi mật khẩu -->
<div class="modal fade" id="passModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel1">Change password</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <div class="modal-body">
                                            
                                                <form>
                                                
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Old password</label>
                                                        <input type="password"  class="form-control" id="password_edit" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">New password</label>
                                                        <input type="password" class="form-control" id="new_password_edit" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Confirm new password</label>
                                                        <input type="password" class="form-control" id="confirm_password_edit">
                                                    </div>
                                                    <input type="hidden" name="user_id" id="user_id" value="{{$user->username}}"/>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-info" id="save">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Chỉnh sửa người dung -->

@endsection
@section('script')
<script>
$(document).ready(function() {
    @if(session('noti'))
    $.toast({
            heading: '{{session('noti')}}',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'success',
            hideAfter: 5000, 
            stack: 6,
            loader: false,
          });
    @endif
    jQuery('#date_birth').datepicker({
        toggleActive: false,
        format: 'yyyy-mm-dd',
        autoclose: true,
        startView: 2,
    });
    $('#save').click(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var username = $('#user_id').val();
        var password = $('#password_edit').val();
        var new_password = $('#new_password_edit').val();
        var confirm_password = $('#confirm_password_edit').val();
        $.ajax({
            type: 'post',
            url: 'users/change-password/',
            data: {
                password: password, new_password: new_password, username: username
            },
            beforeSend: function(){
                
                if(password == "" || new_password == ""){
                    $.toast({
                        heading: 'Error',
                        text: 'Please enter all field',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'warning',
                        hideAfter: 5000, 
                        stack: 6,
                        loader: false,
                    });
                    return false;
                }
                if(new_password != confirm_password){
                    $.toast({
                        heading: 'Error',
                        text: 'Incorrect old password',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'warning',
                        hideAfter: 5000, 
                        stack: 6,
                        loader: false,
                    });
                    return false;
                }
                $('.preloader').fadeIn();
            },
            success: function(resp){
                $('.preloader').fadeOut();
                if(resp == "ok"){
                    
                    $.toast({
                        heading: 'Save success',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'success',
                        hideAfter: 3000, 
                        stack: 6,
                        loader: false,
                    });
                    setTimeout('window.location.href = "users/detail/'+username+'";',3000);
                } else {
                    $.toast({
                        heading: 'Lỗi',
                        text: 'Incorrect old password',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'warning',
                        hideAfter: 5000, 
                        stack: 6,
                        loader: false,
                    });
                }
            }
        })
    })
})
</script>
@endsection