@extends('layout.main')
@section('css')
<link href="assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />

@endsection
@section('js')
<!-- Date Picker Plugin JavaScript -->
<script src="assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
@endsection
@section('content')
<!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">User List</h3>
                        
                        </ol>
                    </div>

                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block">
                            <button style="margin-bottom:20px;" type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Add</button>
                            <table class="table" id="myTable">
                                        <thead>
                                            <tr>
                                                <th>Username</th>
                                                <th>Name</th>
                                                <th>Date of birth</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>Address</th>
                                                <th>Role</th>
                                                <th>Action</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($user as $us)
                                            <tr>
                                                <td>{{$us->username}}</td>
                                                <td>{{$us->name}}</td>
                                                <td>{{$us->date_of_birth}}</td>
                                                <td>{{$us->email}}</td>
                                                <td>{{$us->phone}}</td>
                                                <td>{{$us->address}}</td>
                                                @if($us->role == 1)
                                                <td><span class="label label-success">Admin</span></td>
                                                @endif
                                                @if($us->role == 2)
                                                <td><span class="label label-info">Member</span></td>
                                                @endif
                                                <td class="text-nowrap">
                                                    <a data-toggle="modal" class="edit_user" data-target="#exampleModal1" data-whatever="@mdo" id="{{$us->id}}"> <i style="margin-right:0px;" data-toggle="tooltip" data-original-title="Edit" data-animation="false" class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                                    <a href="users/delete/{{$us->id}}" onclick="javascript:confirmationDelete($(this));return false;" data-toggle="tooltip" data-original-title="Delete" data-animation="false"> <i class="fa fa-close text-danger connect" label="{{$us->id}}"></i> </a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- Thêm người dùng -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel1">Add User</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <div class="modal-body">
                                            
                                                <form>
                                                <div class="alert alert-warning" style="display: none;" id="info"></div>
                                                <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Username</label>
                                                        <input type="text" class="form-control" id="username" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Name</label>
                                                        <input type="text" class="form-control" id="name" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Date of birth</label>
                                                        <div class="input-append date form_datetime">
                                                            <input id="date_of_birth" class="form-control" type="text" name="time" value="" readonly>
                                                        <span class="add-on"><i class="icon-th"></i></span>
                                                    </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Email</label>
                                                        <input type="email" class="form-control" id="email" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Phone</label>
                                                        <input type="number" class="form-control" id="phone" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Address</label>
                                                        <input type="text" class="form-control" id="address" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Password</label>
                                                        <input type="password" class="form-control" id="password" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Password Confirm</label>
                                                        <input type="password" class="form-control" id="confirm_password">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Role</label>
                                                        <select class="form-control p-0" style="width: 100%" name="role" id="role">
                                                        <option value="1">Admin</option>
                                                        <option value="2">Member</option>
                                                        </select>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-info" id="add">Add</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Thêm người dung -->
                                <!-- Chỉnh sửa người dùng -->
                                <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel1">Edit</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <div class="modal-body">
                                            
                                                <form>
                                                <div class="alert alert-warning" style="display: none;" id="info"></div>
                                                <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Username</label>
                                                        <input type="text" class="form-control" id="username_edit" disabled>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Name</label>
                                                        <input type="text" class="form-control" id="name_edit" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Date of birth</label>
                                                        <div class="input-append date form_datetime">
                                                            <input id="date_of_birth_edit" class="form-control" type="text" name="time" value="" readonly>
                                                        <span class="add-on"><i class="icon-th"></i></span>
                                                    </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Email</label>
                                                        <input type="email" class="form-control" id="email_edit" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Phone</label>
                                                        <input type="number" class="form-control" id="phone_edit" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Address</label>
                                                        <input type="text" class="form-control" id="address_edit" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Old Password</label>
                                                        <input type="password" placeholder="leave blank if not change password" class="form-control" id="password_edit" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">New Password</label>
                                                        <input type="password" class="form-control" id="new_password_edit" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">New Password Confirm</label>
                                                        <input type="password" class="form-control" id="confirm_password_edit">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Role</label>
                                                        <select class="form-control p-0" style="width: 100%" name="role" id="role_edit">
                                                        <option value="1">Admin</option>
                                                        <option value="2">Member</option>
                                                        </select>
                                                    </div>
                                                    <input type="hidden" name="user_id" id="user_id" />
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-info" id="save">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Chỉnh sửa người dung -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
@endsection
@section('script')
<script>
$(document).ready(function() {
    function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};
$('#myTable').DataTable(
            {
        
    }
        );
        jQuery('.form_datetime').datepicker({
        toggleActive: false,
        format: 'yyyy-mm-dd',
        autoclose: true,
        startView: 2,
    });
        @if(session('noti'))
    $.toast({
            heading: '{{session('noti')}}',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'success',
            hideAfter: 5000, 
            stack: 6,
            loader: false,
          });
    @endif
    //Thêm người dùng
    $('#add').click(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var username = $('#username').val();
        var name = $('#name').val();
        var birth = $('#date_of_birth').val();
        var email = $('#email').val();
        var password = $('#password').val();
        var confirm_password = $('#confirm_password').val();
        var role = $('#role').val();
        var phone = $('#phone').val();
        var address = $('#address').val();
        $.ajax({
            type: 'post',
            url: 'users/add',
            data: {
                username: username, name: name, email: email, password: password, role: role, phone: phone, address: address, date_of_birth: birth
            },
            beforeSend: function(){
                
                if(username == "" || name == "" || password == "" || email == "" || phone == "" || address == "" || birth == ""){
                    $.toast({
                        heading: 'Error',
                        text: 'Please enter all fields',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'warning',
                        hideAfter: 5000, 
                        stack: 6,
                        loader: false,
                    });
                    return false;
                }
                if(password != confirm_password){
                    $.toast({
                        heading: 'Error',
                        text: 'Incorrect password confirm',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'warning',
                        hideAfter: 5000, 
                        stack: 6,
                        loader: false,
                    });
                    return false;
                }
                if(validateEmail(email) == 0){
                    $.toast({
                        heading: 'Error',
                        text: 'Invalid Email',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'warning',
                        hideAfter: 5000, 
                        stack: 6,
                        loader: false,
                    });
                    return false;
                }
                $('.preloader').fadeIn();
            },
            success: function(resp){
                if(resp == "ok"){
                    $('.preloader').fadeOut();
                    $.toast({
                        heading: 'Add success',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'success',
                        hideAfter: 1500, 
                        stack: 6,
                        loader: false,
                    });
                    setTimeout('window.location.href = "users";',1500);

                } else {
                    $('.preloader').fadeOut();
                    $.toast({
                        heading: 'Error',
                        text: 'Username already exists',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'warning',
                        hideAfter: 5000, 
                        stack: 6,
                        loader: false,
                    });
                }
            }
        })
    })
    //Chỉnh sửa người dùng
    $("#myTable").on("click", ".edit_user", function(){
// Lấy thông tin người dùng
        var id = $(this).attr("id");
        $.ajax({
            type: 'get',
            url: 'users/edit/'+id,
            dataType: 'json',
            data:{
                id: id
            },
            success: function(resp){
                $('.preloader').fadeOut();
                $('#username_edit').val(resp.username);
                $('#name_edit').val(resp.name);
                $('#date_of_birth_edit').val(resp.date_of_birth);
                $('#email_edit').val(resp.email);
                $('#phone_edit').val(resp.phone);
                $('#address_edit').val(resp.address);
                $('#role_edit').val(resp.role);
                $('#user_id').val(resp.id);
            }
        })
    })
    // Lưu thông tin chỉnh sửa


    $('#save').click(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var id = $('#user_id').val();
        var name = $('#name_edit').val();
        var birth = $('#date_of_birth_edit').val();
        var email = $('#email_edit').val();
        var phone = $('#phone_edit').val();
        var address = $('#address_edit').val();
        var password = $('#password_edit').val();
        var new_password = $('#new_password_edit').val();
        var confirm_password = $('#confirm_password_edit').val();
        var role = $('#role_edit').val();
        $.ajax({
            type: 'post',
            url: 'users/edit/'+id,
            data: {
                id: id, name: name, email: email, password: password, role: role, new_password: new_password, phone: phone, address: address, date_of_birth: birth
            },
            beforeSend: function(){
                
                if(username == "" || name == "" || email == "" || phone == "" || address == "" || birth == ""){
                    $.toast({
                        heading: 'Error',
                        text: 'Please enter all fields',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'warning',
                        hideAfter: 5000, 
                        stack: 6,
                        loader: false,
                    });
                    return false;
                }
                if(password != "" && new_password == ""){
                    $.toast({
                        heading: 'Error',
                        text: 'Please enter new password',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'warning',
                        hideAfter: 5000, 
                        stack: 6,
                        loader: false,
                    });
                    return false;
                }
                if(new_password != confirm_password){
                    $.toast({
                        heading: 'Error',
                        text: 'Incorrect password confirm',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'warning',
                        hideAfter: 5000, 
                        stack: 6,
                        loader: false,
                    });
                    return false;
                }
                if(validateEmail(email) == 0){
                    $.toast({
                        heading: 'Error',
                        text: 'Invalid Email',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'warning',
                        hideAfter: 5000, 
                        stack: 6,
                        loader: false,
                    });
                    return false;
                }
                $('.preloader').fadeIn();
            },
            success: function(resp){
                $('.preloader').fadeOut();
                if(resp == "ok"){
                    
                    $.toast({
                        heading: 'Save success',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'success',
                        hideAfter: 1500, 
                        stack: 6,
                        loader: false,
                    });
                    setTimeout('window.location.href = "users";',1500);

                } else {
                    $.toast({
                        heading: 'Error',
                        text: 'Old password incorrect',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'warning',
                        hideAfter: 5000, 
                        stack: 6,
                        loader: false,
                    });
                }
            }
        })
    })
})
function confirmationDelete(anchor) {
    var conf = swal({   
        title: "Are you sure?",      
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Yes",
        cancelButtonText: "No",   
        closeOnConfirm: false 
    }, function(){   
        window.location = anchor.attr("href"); 
    });
    
    
}
function formatNumber(num) {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}
</script>
@endsection