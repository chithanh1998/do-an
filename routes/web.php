<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('login', 'UsersController@post_login');
Route::get('logout', 'UsersController@logout');
Route::get('/', 'UsersController@login');
Route::get('search', 'SearchController@search')->middleware('login');

    Route::group(['prefix' => 'users','middleware'=>['login']], function () {
        Route::get('/', 'UsersController@list')->middleware('admin');
        Route::post('add', 'UsersController@post_add_user')->middleware('admin');
        Route::get('edit/{id}', 'UsersController@get_edit_user')->middleware('admin');
        Route::post('edit/{id}', 'UsersController@post_edit_user')->middleware('admin');
        Route::get('delete/{id}', 'UsersController@get_delete_user')->middleware('admin');
        Route::get('detail/{username}', 'UsersController@get_detail_user');
        Route::post('detail/{username}', 'UsersController@post_detail_user');
        Route::post('change-password','UsersController@post_pass_user');
    });
    Route::group(['prefix' => 'bus','middleware'=>['login']], function () {
        Route::get('/', 'BusController@list');
        Route::get('type', 'BusController@type_list');
        Route::post('add', 'BusController@add_bus')->middleware('admin');
        Route::get('edit/{id}', 'BusController@get_edit_bus');
        Route::post('edit/{id}', 'BusController@post_edit_bus');
        Route::get('delete/{id}', 'BusController@delete_bus')->middleware('admin');
        Route::post('type/add', 'BusController@add_bus_type')->middleware('admin');
        Route::get('type/delete/{id}', 'BusController@delete_bus_type')->middleware('admin');
    });
    Route::group(['prefix' => 'drivers','middleware'=>'login'], function () {
        Route::get('/', 'DriversController@list');
        Route::post('add', 'DriversController@add_driver')->middleware('admin');
        Route::get('edit/{id}', 'DriversController@get_edit_driver');
        Route::post('edit/{id}', 'DriversController@post_edit_driver');
        Route::get('delete/{id}', 'DriversController@delete_driver')->middleware('admin');
    });
    Route::group(['prefix' => 'schedule','middleware'=>'login'], function () {
        Route::get('/', 'ScheduleController@list');
        Route::post('add', 'ScheduleController@add_schedule');
        Route::get('edit/{id}', 'ScheduleController@get_edit_schedule');
        Route::post('edit/{id}', 'ScheduleController@post_edit_schedule');
        Route::get('delete/{id}', 'ScheduleController@delete_schedule');
        Route::get('date', 'ScheduleController@list_filter');
        Route::get('detail/{id}', 'ScheduleController@detail');
        Route::get('detail/{id}/date', 'ScheduleController@detail_filter');

    });
    Route::group(['prefix' => 'dashboard','middleware'=>'login'], function () {
        Route::get('/', 'DashboardController@month');
        Route::get('year', 'DashboardController@year');
        Route::get('all', 'DashboardController@all');
    });
